REM
REM bmp2dat
REM Last update: 28 Sep 2010
REM
REM Converts a BMP file (MS format) into a RLE-compressed graphic file (Mateusz' specific format)
REM Warning: BMP 4-bytes alignement per row is not supported! Will work only on bitmaps with xSize MOD 4 = 0.
REM

DIM MagicBytes AS STRING*2
DIM AS UINTEGER DataOffset, xSize, ySize, CompMethod   ' 32 bits values
DIM AS USHORT ColorDepth   ' 16 bits values
DIM AS INTEGER x, y
DIM AS UBYTE RLEval, NonRLEbuffSize
DIM AS STRING FileIN, FileOUT, NonRLEbuff
DIM AS SHORT DwordBuff

TYPE PixelColor
  R AS UBYTE
  G AS UBYTE
  B AS UBYTE
END TYPE

DIM CurPixel AS PixelColor

FileIN = COMMAND(1)
FileOUT = COMMAND(2)

IF LEN(FileIN) = 0 OR LEN(FileOUT) = 0 THEN
  PRINT "Usage: bmp2dat infile.bmp outfile.dat"
  END(1)
END IF

PRINT "Input file:  "; FileIN
PRINT "Output file: "; FileOUT

OPEN FileIN FOR BINARY AS #1

GET #1, 1, MagicBytes
PRINT "Magic bytes: "; MagicBytes
IF MagicBytes <> "BM" THEN
  PRINT "ERROR: Not a valid BMP file!"
  CLOSE #1
  END(1)
END IF

GET #1, &hB, DataOffset
PRINT "Data Offset: " & DataOffset

GET #1, &h13, xSize
GET #1, &h17, ySize
PRINT "Resolution: " & xSize & "x" & ySize
IF xSize < 1 OR ySize < 1 THEN
  PRINT "ERROR: Invalid resolution!"
  CLOSE #1
  END(1)
END IF
IF xSize MOD 4 <> 0 OR ySize MOD 4 <> 0 THEN
  PRINT "ERROR: Unsupported resolution. Must be a multiple of 4 for both x and y dimensions."
  CLOSE #1
  END(1)
END IF

DIM SHARED WorkImg(1 TO xSize, 1 TO ySize) AS PixelColor

GET #1, &h1D, ColorDepth
PRINT "ColorDepth: " & ColorDepth
IF ColorDepth <> 24 THEN
  PRINT "ERROR: Usupported color mode. Only 24bpp is supported."
  CLOSE #1
  END(1)
END IF

GET #1, &h1F, CompMethod
PRINT "Compression: ";
IF CompMethod <> 0 THEN
    PRINT "yes"
    PRINT "ERROR: Compressed BMP files are not supported!"
    END(1)
    CLOSE #1
  ELSE
    PRINT "no"
END IF

PRINT
PRINT "Converting... ";
'PRINT "Press any key to start the conversion of the image..."
'SLEEP

'SCREENRES 1024, 768, 32
SEEK #1, DataOffset + 1

FOR y = ySize TO 1 STEP -1
  FOR x = 1 TO xSize
    GET #1,, CurPixel.B
    GET #1,, CurPixel.G
    GET #1,, CurPixel.R
    'PSET (x + 20, y + 20), RGB(CurPixel.R, CurPixel.G, CurPixel.B)
    WorkImg(x, y).R = CurPixel.R
    WorkImg(x, y).G = CurPixel.G
    WorkImg(x, y).B = CurPixel.B
  NEXT x
NEXT y

CLOSE #1

OPEN FileOUT FOR OUTPUT AS #1
PUT #1,, "MateuszBitmapData"
PUT #1,, xSize
PUT #1,, ySize

RLEval = 0
CurPixel.R = WorkImg(1, 1).R
CurPixel.G = WorkImg(1, 1).G
CurPixel.B = WorkImg(1, 1).B

FOR y = 1 TO ySize
  FOR x = 1 TO xSize
    IF WorkImg(x, y).R = CurPixel.R AND WorkImg(x, y).G = CurPixel.G AND WorkImg(x, y).B = CurPixel.B AND RLEval < 255 THEN
        RLEval += 1
      ELSE
        IF RLEval = 1 THEN
            DwordBuff = ((CurPixel.R \ 8) SHL 10) OR ((CurPixel.G \ 8) SHL 5) OR (CurPixel.B \ 8)
            PUT #1,, DwordBuff
          ELSE
            DwordBuff = &b1000000000000000 OR ((CurPixel.R \ 8) SHL 10) OR ((CurPixel.G \ 8) SHL 5) OR (CurPixel.B \ 8)
            PUT #1,, DwordBuff
            PUT #1,, RLEval
        END IF
        RLEval = 1
    END IF
    CurPixel = WorkImg(x, y)
  NEXT x
NEXT y

IF RLEval = 1 THEN
    DwordBuff = ((CurPixel.R \ 8) SHL 10) OR ((CurPixel.G \ 8) SHL 5) OR (CurPixel.B \ 8)
    PUT #1,, DwordBuff
  ELSE
    DwordBuff = &b1000000000000000 OR ((CurPixel.R \ 8) SHL 10) OR ((CurPixel.G \ 8) SHL 5) OR (CurPixel.B \ 8)
    PUT #1,, DwordBuff
    PUT #1,, RLEval
END IF

CLOSE #1

PRINT "Done!"
