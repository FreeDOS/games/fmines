#INCLUDE ONCE "core.bi"
#INCLUDE ONCE "loadmateuszgraphic.bi"

DIM SHARED AS INTEGER xMouse, yMouse, MouseButtons, MouseClickLeft, MouseClickRight
DIM SHARED AS INTEGER xStart = 60, yStart = 60, BlockSize
DIM SHARED AS BYTE GameLevel ' 1 = EASY ; 2 = NORMAL ; 3 = HARD
DIM SHARED AS BYTE QuitFlag  ' 1 = Soft quit (return to main menu) ; 2 = Hard quit (exit the game)
DIM SHARED RefreshBlockCache(1 TO 32, 1 TO 32) AS BYTE
DIM SHARED BigImgBuffer AS ANY PTR
DIM SHARED BackgroundBuff AS ANY PTR
DIM SHARED MenuImgBuffer AS ANY PTR
DIM SHARED PauseScreen AS ANY PTR
DIM SHARED BackgrPlay AS ANY PTR
DIM SHARED ThemesList(1 TO 128) AS STRING
DIM SHARED AS INTEGER ThemesListSize, CurTheme
CONST AS STRING pVer = "1.00"
CONST AS STRING pDate = "2010"

TYPE ConfigurationFile
  CurThemeName AS STRING
  FullScreenParam AS BYTE
  Changed AS BYTE
END TYPE
DIM SHARED ConfigFile AS ConfigurationFile

#IFDEF __FB_DOS__
  CONST DirSeparator AS STRING = "\"
#ELSE
  CONST DirSeparator AS STRING = "/"
#ENDIF

#IFDEF __FB_WIN32__  ' Use the GDI driver on Windows instead of DirectX
  #INCLUDE ONCE "fbgfx.bi"
  SCREENCONTROL(fb.SET_DRIVER_NAME, "GDI")
#ENDIF

#IFNDEF __FB_DOS__   ' Set window's title, if not under DOS
  WINDOWTITLE("FancyMines v" + pVer)
#ENDIF

' Here start all subs and procedures...

SUB DrawBlock(x AS INTEGER, y AS INTEGER, BlockType AS BYTE)
  DIM AS INTEGER RealX, RealY, BlockAddress
  SELECT CASE GameLevel
    CASE 1 ' EASY, blocks are 80x80, and minefield 8x8
      BlockAddress = 300
    CASE 2 ' NORMAL, blocks are 40x40, and minefield 16x16
      BlockAddress = 0
    CASE 3 ' HARD, blocks are 20x20, and minefield 32x32
      BlockAddress = 50
  END SELECT
  RealX = ((x - 1) * BlockSize) + xStart
  RealY = ((y - 1) * BlockSize) + yStart
  IF BlockType >= 0 AND BlockType <= 8 THEN
      LINE (RealX, RealY)-(RealX + BlockSize - 1, RealY + BlockSize - 1), RGB(255,255,255), BF  ' Make the block white
      PUT(RealX, RealY), BackgroundBuff, (RealX - xStart, RealY - yStart)-STEP(BlockSize - 1, BlockSize - 1), ALPHA, 96
  END IF
  SELECT CASE BlockType
    CASE -64 ' Original background
      PUT(RealX, RealY), BackgroundBuff, (RealX - xStart, RealY - yStart)-STEP(BlockSize - 1, BlockSize - 1), PSET
    CASE -3 ' Flagged
      PUT(RealX, RealY), BigImgBuffer, (BlockSize * 11, BlockAddress)-STEP(BlockSize - 1, BlockSize - 1), TRANS ' Hidden field
      PUT(RealX, RealY), BigImgBuffer, (BlockSize * 13, BlockAddress)-STEP(BlockSize - 1, BlockSize - 1), TRANS ' Flag
    CASE -2 ' Question-marked field
      PUT(RealX, RealY), BigImgBuffer, (BlockSize * 11, BlockAddress)-STEP(BlockSize - 1, BlockSize - 1), TRANS ' Hidden field
      PUT(RealX, RealY), BigImgBuffer, (BlockSize * 14, BlockAddress)-STEP(BlockSize - 1, BlockSize - 1), TRANS ' Question mark field
    CASE -1 ' Hidden field
      PUT(RealX, RealY), BigImgBuffer, (BlockSize * 11, BlockAddress)-STEP(BlockSize - 1, BlockSize - 1), TRANS
    CASE 0  ' Empty field
      PUT(RealX, RealY), BigImgBuffer, (0, BlockAddress)-STEP(BlockSize - 1, BlockSize - 1), TRANS
    CASE 1
      PUT(RealX, RealY), BigImgBuffer, (BlockSize, BlockAddress)-STEP(BlockSize - 1, BlockSize - 1), TRANS
    CASE 2
      PUT(RealX, RealY), BigImgBuffer, (BlockSize * 2, BlockAddress)-STEP(BlockSize - 1, BlockSize - 1), TRANS
    CASE 3
      PUT(RealX, RealY), BigImgBuffer, (BlockSize * 3, BlockAddress)-STEP(BlockSize - 1, BlockSize - 1), TRANS
    CASE 4
      PUT(RealX, RealY), BigImgBuffer, (BlockSize * 4, BlockAddress)-STEP(BlockSize - 1, BlockSize - 1), TRANS
    CASE 5
      PUT(RealX, RealY), BigImgBuffer, (BlockSize * 5, BlockAddress)-STEP(BlockSize - 1, BlockSize - 1), TRANS
    CASE 6
      PUT(RealX, RealY), BigImgBuffer, (BlockSize * 6, BlockAddress)-STEP(BlockSize - 1, BlockSize - 1), TRANS
    CASE 7
      PUT(RealX, RealY), BigImgBuffer, (BlockSize * 7, BlockAddress)-STEP(BlockSize - 1, BlockSize - 1), TRANS
    CASE 8
      PUT(RealX, RealY), BigImgBuffer, (BlockSize * 8, BlockAddress)-STEP(BlockSize - 1, BlockSize - 1), TRANS
    CASE 9 ' BOMB
      PUT(RealX, RealY), BigImgBuffer, (BlockSize * 11, BlockAddress)-STEP(BlockSize - 1, BlockSize - 1), TRANS ' Hidden field
      PUT(RealX, RealY), BigImgBuffer, (BlockSize * 9, BlockAddress)-STEP(BlockSize - 1, BlockSize - 1), TRANS ' Bomb
    CASE 10 ' False bomb
      PUT(RealX, RealY), BigImgBuffer, (BlockSize * 11, BlockAddress)-STEP(BlockSize - 1, BlockSize - 1), TRANS ' Hidden field
      PUT(RealX, RealY), BigImgBuffer, (BlockSize * 12, BlockAddress)-STEP(BlockSize - 1, BlockSize - 1), TRANS ' False bomb
    CASE 11 ' Exploded Bomb
      PUT(RealX, RealY), BigImgBuffer, (BlockSize * 10, BlockAddress)-STEP(BlockSize - 1, BlockSize - 1), TRANS ' Exploded bomb
  END SELECT
END SUB

SUB PrintString(xp AS INTEGER, yp AS INTEGER, DataString AS STRING)
  DIM AS INTEGER x, FontPos, FontWidth = 25
  DIM AS STRING CharBuff
  FOR x = 1 TO LEN(DataString)
    CharBuff = MID(DataString, x, 1)
    SELECT CASE CharBuff
      CASE "0"
        FontPos = 61
      CASE ":"
        FontPos = 62
      CASE "1","2","3","4","5","6","7","8","9"
        FontPos = ASC(CharBuff) + 3
      CASE "a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"
        FontPos = ASC(CharBuff) - 97
      CASE "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"
        FontPos = ASC(CharBuff) - 39
      CASE ELSE ' For non-available glyphs...
        FontPos = -1
    END SELECT
    IF FontPos >= 0 THEN PUT (xp + ((x - 1) * FontWidth), yp), BigImgBuffer, (FontPos * FontWidth, 220)-STEP(FontWidth - 1, 45), TRANS
  NEXT x
END SUB

FUNCTION GetMouseOverBlockX() AS INTEGER
  DIM AS INTEGER BlockX
  BlockX = INT((xMouse - xStart - 1) / (BlockSize)) + 1
  RETURN BlockX
END FUNCTION

FUNCTION GetMouseOverBlockY() AS INTEGER
  DIM AS INTEGER BlockY
  BlockY = INT((yMouse - yStart - 1) / (BlockSize)) + 1
  RETURN BlockY
END FUNCTION

SUB ShowFullBackground()
  DIM AS INTEGER x, y, z, t, u, xDone
  DIM AS DOUBLE SleepDuration, LastBlockTime
  x = GetFieldWidth() / 2
  y = GetFieldHeight() / 2
  t = 1
  u = 1
  SleepDuration = 0.01 ' how much time (in seconds) should be waited between each block drawing?
  DrawBlock(x, y, -64)
  DO
    FOR z = 1 TO t
      IF x + u > GetFieldWidth() OR x + u < 1 THEN
          xDone = 1
        ELSE
          x += u
          WHILE ABS(TIMER - LastBlockTime) < SleepDuration
            SLEEP 1, 1
          WEND
          LastBlockTime = TIMER
          DrawBlock(x, y, -64)
      END IF
    NEXT z
    FOR z = 1 TO t
      IF y + u <= GetFieldHeight() AND y + u > 0 THEN
        y += u
        WHILE ABS(TIMER - LastBlockTime) < SleepDuration
          SLEEP 1, 1
        WEND
        LastBlockTime = TIMER
        DrawBlock(x, y, -64)
      END IF
    NEXT z
    t += 1
    u = 0 - u
  LOOP UNTIL xDone = 1
END SUB

SUB RefreshScreen(ForceFullRefresh AS BYTE = 0)
  DIM AS INTEGER x, y, xx, yy, sMinutes, sSeconds, TimeElapsed
  TimeElapsed = GetGameDuration()
  sMinutes = INT(TimeElapsed / 60)
  sSeconds = TimeElapsed - (60 * sMinutes)
  xx = GetFieldWidth()
  yy = GetFieldHeight()
  IF TimeElapsed < 60 THEN
      PrintString(760, 285, "" & sSeconds & "s")
    ELSE
      PrintString(760, 285, "" & sMinutes & "m" & RIGHT("00" & sSeconds, 2) & "s")
  END IF
  PrintString(830, 500, RIGHT("0" & GetMinesLeft(), 2))
  FOR y = 1 TO yy
    FOR x = 1 TO xx
      IF RefreshBlockCache(x, y) <> GetField(x, y) OR ForceFullRefresh = 1 THEN
        DrawBlock(x, y, GetField(x, y))
        RefreshBlockCache(x, y) = GetField(x, y)
      END IF
    NEXT x
  NEXT y
END SUB

SUB UpdateMouseStatus()
  GetMouse(xMouse, yMouse,, MouseButtons)
  IF MouseButtons AND 1 AND xMouse > 0 THEN
      MouseClickLeft = 1
    ELSE
      IF MouseClickLeft = 1 THEN MouseClickLeft = -1 ELSE MouseClickLeft = 0
  END IF
  IF MouseButtons AND 2 AND xMouse > 0 THEN
      MouseClickRight = 1
    ELSE
      IF MouseClickRight = 1 THEN MouseClickRight = -1 ELSE MouseClickRight = 0
  END IF
END SUB

SUB CheckMousePresence()
  DIM AS INTEGER x, y, z
  GetMouse(x, y,, z)
  IF x < 0 OR y < 0 THEN
    PRINT "No mouse has been detected! Please load a mouse driver."
    END(1)
  END IF
END SUB

SUB LoadGraphicTheme()
  DIM AS INTEGER LoadResult, FirstTime = 1
  IF BigImgBuffer <> 0 THEN
    PUT(262, 250), BigImgBuffer, (700, 0)-STEP(500, 200), TRANS ' Show "Loading..."
    FirstTime = 0
  END IF
  LoadResult = LoadMateuszGraphic(EXEPATH + DirSeparator + ThemesList(CurTheme) + DirSeparator + "image.dat", BigImgBuffer)
  IF LoadResult <> 0 THEN
      PRINT "Error: file image.dat couldn't be loaded! [" & LoadResult & "]"
      SLEEP 1000, 1
    ELSE
      PUT(262, 250), BigImgBuffer, (700, 0)-STEP(500, 200), TRANS ' Show "Loading..."
  END IF
  LoadResult = LoadMateuszGraphic(EXEPATH + DirSeparator + ThemesList(CurTheme) + DirSeparator + "menu.dat", MenuImgBuffer)
  IF LoadResult <> 0 THEN
      PRINT "Error: file menu.dat couldn't be loaded! [" & LoadResult & "]"
      SLEEP 1000, 1
  END IF
  LoadResult = LoadMateuszGraphic(EXEPATH + DirSeparator + ThemesList(CurTheme) + DirSeparator + "mine000.dat", PauseScreen)
  IF LoadResult <> 0 THEN
    PRINT "Error: file " & ThemesList(CurTheme) & DirSeparator & "mine000.dat couldn't be loaded! [" & LoadResult & "]"
    SLEEP 1000, 1
  END IF
  LoadResult = LoadMateuszGraphic(EXEPATH + DirSeparator + ThemesList(CurTheme) + DirSeparator + "back.dat", BackgrPlay)
  IF LoadResult <> 0 THEN
    PRINT "Error: file " & ThemesList(CurTheme) & DirSeparator & "back.dat couldn't be loaded! [" & LoadResult & "]"
    SLEEP 1000, 1
  END IF
END SUB

SUB ChooseGameLevel()
  DIM AS STRING LastKey
  GameLevel = 0
  PUT (0,0), MenuImgBuffer, PSET
  DO
    SLEEP 1, 1
    UpdateMouseStatus()
    LastKey = INKEY
    IF LastKey = CHR(255) + "k" OR LastKey = CHR(27) THEN QuitFlag = 2
    IF MouseClickLeft = -1 THEN
      IF xMouse < 680 AND xMouse > 350 THEN
        IF yMouse > 290 AND yMouse < 380 THEN GameLevel = 1
        IF yMouse > 400 AND yMouse < 490 THEN GameLevel = 2
        IF yMouse > 510 AND yMouse < 600 THEN GameLevel = 3
        IF yMouse > 620 AND yMouse < 705 THEN QuitFlag = 2
      END IF
      IF xMouse > 880 AND xMouse < 1017 AND yMouse > 725 AND yMouse < 760 AND ThemesListSize > 1 THEN
        IF CurTheme = ThemesListSize THEN CurTheme = 1 ELSE CurTheme += 1
        LoadGraphicTheme()
        PUT (0,0), MenuImgBuffer, PSET
        ConfigFile.CurThemeName = ThemesList(CurTheme)
        ConfigFile.Changed = 1
      END IF
    END IF
  LOOP UNTIL QuitFlag = 2 OR GameLevel > 0
END SUB

SUB PlayGame()
  DIM AS INTEGER ClickedBlockX, ClickedBlockY, x, y, LoadResult, BackgroundsListSize
  DIM BackgroundsList(1 TO 100) AS STRING
  DIM AS STRING LastKey, BackgroundFile
  DIM AS DOUBLE LastRefresh

  BackgroundsListSize = 1
  BackgroundsList(1) = DIR(EXEPATH + DirSeparator + ThemesList(CurTheme) + DirSeparator + "mine" & GameLevel & "??.dat")
  DO
    BackgroundsList(BackgroundsListSize + 1) = DIR()
    IF LEN(BackgroundsList(BackgroundsListSize + 1)) > 0 THEN
        BackgroundsListSize += 1
      ELSE
        EXIT DO
    END IF
  LOOP UNTIL BackgroundsListSize = 100
  BackgroundFile = BackgroundsList(INT(RND * BackgroundsListSize) + 1)
  SELECT CASE GameLevel
    CASE 1 ' Easy
      NewGameInit(8, 8, 10)
      BlockSize = 80
    CASE 2 ' Normal
      NewGameInit(16, 16, 40)
      BlockSize = 40
    CASE 3 ' Hard
      NewGameInit(32, 32, 200)
      BlockSize = 20
  END SELECT

  PUT(262, 250), BigImgBuffer, (700, 0)-STEP(500, 200), TRANS ' Loading...
  SLEEP 300, 1
  LoadResult = LoadMateuszGraphic(EXEPATH + DirSeparator + ThemesList(CurTheme) + DirSeparator + BackgroundFile, BackgroundBuff)
  IF LoadResult <> 0 THEN
    PRINT "Error: file " & ThemesList(CurTheme) & DirSeparator & BackgroundFile & " couldn't be loaded! [" & LoadResult & "]"
    SLEEP 1000, 1
  END IF

  PUT (0, 0), BackgrPlay, PSET  ' Background of the play menu
  FOR x = 1 TO GetFieldWidth()
    FOR y = 1 TO GetFieldHeight()
      RefreshBlockCache(x, y) = -100
    NEXT y
  NEXT x
  RefreshScreen(1) ' Force drawing all blocks

  DO
    LastKey = INKEY
    SLEEP 1, 1
    UpdateMouseStatus()
    IF MouseClickLeft = 1 AND ClickedBlockX = 0 AND ClickedBlockY = 0 THEN
      ClickedBlockX = GetMouseOverBlockX()
      ClickedBlockY = GetMouseOverBlockY()
    END IF
    IF MouseClickLeft = -1 THEN
        IF GetMouseOverBlockX() = ClickedBlockX AND GetMouseOverBlockY() = ClickedBlockY THEN
          IF ClickedBlockX >= 1 AND ClickedBlockX <= GetFieldWidth() AND ClickedBlockY >= 1 AND ClickedBlockY <= GetFieldHeight() AND GetField(ClickedBlockX, ClickedBlockY) = -1 THEN
            SetField(ClickedBlockX, ClickedBlockY, 3)
          END IF
        END IF
        ClickedBlockX = 0 : ClickedBlockY = 0
    END IF
    IF MouseClickRight = 1 AND ClickedBlockX = 0 AND ClickedBlockY = 0 THEN
      ClickedBlockX = GetMouseOverBlockX()
      ClickedBlockY = GetMouseOverBlockY()
    END IF
    IF MouseClickRight = -1 THEN
        IF GetMouseOverBlockX() = ClickedBlockX AND GetMouseOverBlockY() = ClickedBlockY THEN
          IF ClickedBlockX >= 1 AND ClickedBlockX <= GetFieldWidth() AND ClickedBlockY >= 1 AND ClickedBlockY <= GetFieldHeight() THEN
            SELECT CASE GetField(ClickedBlockX, ClickedBlockY)
              CASE -1  ' Just hidden
                SetField(ClickedBlockX, ClickedBlockY, 1) ' Mark with flag
              CASE -2  ' Flagged with Question mark
                SetField(ClickedBlockX, ClickedBlockY, 2) ' Unmark
              CASE -3  ' Flaged
                SetField(ClickedBlockX, ClickedBlockY, 0) ' Flag with question mark
            END SELECT
          END IF
        END IF
        ClickedBlockX = 0 : ClickedBlockY = 0
    END IF
    IF LastKey = CHR(255) + "k" OR LastKey = CHR(27) THEN QuitFlag = 2 ' "Hard" quit
    IF UCASE(LastKey) = "P" AND GetGameOverFlag() = 0 THEN ' Pause
      PauseGame()
      FOR y = 0 TO 319 STEP 12
        PUT (xStart, yStart + y), PauseScreen, (0, y)-STEP(639, 12), PSET
        PUT (xStart, yStart + 639 - y - 12), PauseScreen, (0, 639 - y - 12)-STEP(639, 12), PSET
        SLEEP 20, 1
      NEXT y
      DO
        SLEEP 500
        LastKey = INKEY
      LOOP UNTIL LEN(LastKey) <> 0
      IF LastKey = CHR(255) + "k" THEN
          QuitFlag = 2
        ELSE
          LastKey = ""
          RefreshScreen(1) ' Force a full refresh
      END IF
      UnPauseGame()
      DO: LOOP UNTIL LEN(INKEY) = 0   ' Flush keyboard buffer
    END IF
    IF ABS(TIMER - LastRefresh) > 0.1 THEN RefreshScreen() : LastRefresh = TIMER  ' Refresh screen at 10fps
    IF GetGameOverFlag() > 0 THEN
      RefreshScreen()
      SELECT CASE GetGameOverFlag()
        CASE 1 ' You lost!
          PUT(716, 80), BigImgBuffer, (300, 100)-STEP(300, 100), TRANS
        CASE 2 ' You won!
          PUT(716, 80), BigImgBuffer, (0, 100)-STEP(300, 100), TRANS
          SLEEP 1500, 1
          ShowFullBackground()
      END SELECT
      DO
        SLEEP 1, 1
        UpdateMouseStatus()
        LastKey = INKEY()
        IF MouseClickLeft = -1 AND xMouse > 816 AND xMouse < 916 AND yMouse > 140 AND yMouse < 174 THEN QuitFlag = 1
        IF LastKey = CHR(27) OR LastKey = CHR(255) + "k" THEN QuitFlag = 2
      LOOP UNTIL QuitFlag > 0
    END IF
  LOOP UNTIL QuitFlag > 0
END SUB

SUB LoadConfigFile()
  DIM ConfHandler AS INTEGER
  ConfHandler = FREEFILE
  IF OPEN(EXEPATH + DirSeparator + "fmines.cfg", FOR INPUT, AS #ConfHandler) = 0 THEN
      LINE INPUT #ConfHandler, ConfigFile.CurThemeName
      INPUT #ConfHandler, ConfigFile.FullScreenParam
      CLOSE #ConfHandler
    ELSE
      PRINT "Warning: Could not open the configuration file (might be normal if you are running the game for the first time)"
      SLEEP 1000, 1
      ConfigFile.Changed = 1
  END IF
END SUB

SUB SaveConfigFile()
  DIM ConfHandler AS INTEGER
  ConfHandler = FREEFILE
  OPEN EXEPATH + DirSeparator + "fmines.cfg" FOR OUTPUT AS #ConfHandler
  PRINT #ConfHandler, ConfigFile.CurThemeName
  PRINT #ConfHandler, ConfigFile.FullScreenParam
  CLOSE #ConfHandler
END SUB


REM Start of the "main" program


#IFDEF __FB_DOS__
  CheckMousePresence()
#ENDIF

PRINT "FancyMiner v"; pVer; " Copyright (C) Mateusz Viste 2010"
IF LEN(COMMAND(1)) > 0 AND UCASE(COMMAND(1)) <> "--SETFULLSCREEN" AND UCASE(COMMAND(1)) <> "--SETWINDOWED" THEN
  PRINT "Compiled on "; __DATE__; ", "; __TIME__; " with FreeBASIC v"; __FB_VERSION__
  PRINT
  PRINT "To run the game, simply call the 'fminer' executable without any parameter."
#IFNDEF __FB_DOS__   ' If not running on DOS (where windowed mode is not supported)...
  PRINT "If you wish to force the game to start in windowed or fullscreen mode, you"
  PRINT "can use following parameters:"
  PRINT
  PRINT "  --setfullscreen     (changes game's settings to start in fullscreen mode)"
  PRINT "  --setwindowed       (changes game's settings to start in windowed mode)"
  PRINT
#ENDIF
  PRINT "Enjoy!"
  PRINT
  END
END IF

LoadConfigFile()
IF ConfigFile.FullscreenParam = 0 AND UCASE(COMMAND(1)) = "--SETFULLSCREEN" THEN
  ConfigFile.FullscreenParam = 1
  ConfigFile.Changed = 1
END IF
IF ConfigFile.FullscreenParam = 1 AND UCASE(COMMAND(1)) = "--SETWINDOWED" THEN
  ConfigFile.FullscreenParam = 0
  ConfigFile.Changed = 1
END IF

CurTheme = 1
ThemesList(1) = DIR(EXEPATH + DirSeparator + "*", &h10) ' Get all directories (themes)
DO
  IF LEN(ThemesList(ThemesListSize + 1)) > 0 AND LEFT(ThemesList(ThemesListSize + 1), 1) <> "." THEN ThemesListSize += 1
  IF ThemesListSize > 0 AND LEN(ConfigFile.CurThemeName) > 0 THEN
    IF ThemesList(ThemesListSize) = ConfigFile.CurThemeName THEN CurTheme = ThemesListSize
  END IF
  ThemesList(ThemesListSize + 1) = DIR()
LOOP UNTIL ThemesList(ThemesListSize + 1) = "" OR ThemesListSize = 127
IF ThemesListSize = 0 THEN
  PRINT "No graphic theme found! You need at least one."
  END(1)
END IF
IF LEN(ConfigFile.CurThemeName) = 0 THEN
  ConfigFile.CurThemeName = ThemesList(CurTheme)
  ConfigFile.Changed = 1
END IF

IF ConfigFile.FullscreenParam = 0 THEN
    SCREENRES 1024, 768, 32, 1, 0  ' 32bits, 1 page only, windowed
  ELSE
    SCREENRES 1024, 768, 32, 1, 1  ' 32bits, 1 page only, fullscreen
END IF

BigImgBuffer = ImageCreate(1600, 400)
BackgroundBuff = ImageCreate(640, 640)
MenuImgBuffer = ImageCreate(1024, 768)
PauseScreen = ImageCreate(640, 640)
BackgrPlay = ImageCreate(1024, 768)

LoadGraphicTheme()
RANDOMIZE TIMER, 3 ' Init the random generator

DO
  QuitFlag = 0
  ChooseGameLevel()
  IF QuitFlag <> 2 THEN
    QuitFlag = 0
    PlayGame()
  END IF
LOOP UNTIL QuitFlag = 2

ImageDestroy BigImgBuffer     ' Unallocate memory reserved
ImageDestroy MenuImgBuffer
ImageDestroy PauseScreen
ImageDestroy BackgrPlay
ImageDestroy BackgroundBuff   ' for storing graphic images

IF ConfigFile.Changed = 1 THEN SaveConfigFile()

END
