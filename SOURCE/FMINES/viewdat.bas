#INCLUDE ONCE "loadmateuszgraphic.bi"
#INCLUDE ONCE "file.bi"

DIM AS STRING Header = "mateuszbitmapdata"
DIM AS INTEGER xsize, ysize

IF LCASE(COMMAND(1)) = "--help" OR LCASE(COMMAND(1)) = "-h" THEN
  PRINT "Viewer for Mateusz' specific graphic format files"
  PRINT " Usage: viewdat file.dat"
  END(0)
END IF

IF FileExists(COMMAND(1)) = 0 THEN
  PRINT "File "; COMMAND(1); " not found."
  END(1)
END IF

OPEN COMMAND(1) FOR BINARY AS #1
  GET #1, 1, Header
  GET #1,, xsize
  GET #1,, ysize
CLOSE #1

IF Header <> "MateuszBitmapData" OR xsize < 1 OR ysize < 1 THEN
  PRINT "Invalid format."
  END(1)
END IF

DIM ImageBuff AS ANY PTR

SCREENRES xsize, ysize, 32
LoadMateuszGraphic(COMMAND(1))

SLEEP
END
