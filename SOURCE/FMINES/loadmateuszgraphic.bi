REM
REM  FreeBASIC Loader for Mateusz' specific format graphic files
REM
REM  Structure of the file:
REM    01..17    MateuszBitmapData
REM    18..21    x size (4 bytes integer)
REM    22..25    y size (4 bytes integer)
REM    26....    Bitmap data (1 byte of RLE value, then 3 RGB bytes, and so on)
REM    Note: if the RLE value is set to 0, then read the next byte, and do not use
REM          RLE for that many next pixels.
REM
REM Returns zero if everything went fine, non-zero else. Possible return codes are:
REM  1 - Could not open specified file
REM  2 - The file wasn't in the Mateusz' specific format
REM  3 - The file seems to be corrupted
REM  4 - The file is too long (might be corrupted)
REM

FUNCTION LoadMateuszGraphic(GraphFilename AS STRING, TargetImg AS ANY PTR = 0) AS INTEGER
  DIM AS INTEGER FileHandler, xGrSize, yGrSize, xpos, ypos, Result, RepeatCount
  DIM AS UBYTE RLEval, PixRedVal, PixGreenVal, PixBlueVal, SkipRLE
  DIM AS STRING FileMagicHeader = "                 "
  DIM AS SHORT DwordBuff  ' 16 bit value  [Rrrrrrgggggbbbbb]
  TYPE PixelCol
    R AS UBYTE
    G AS UBYTE
    B AS UBYTE
  END TYPE
  DIM PixVal AS PixelCol
  FileHandler = FREEFILE
  Result = 0
  IF OPEN(GraphFilename, FOR BINARY, AS #FileHandler) = 0 THEN
      GET #FileHandler, 1, FileMagicHeader
      IF FileMagicHeader = "MateuszBitmapData" THEN
          GET #FileHandler,, xGrSize
          GET #FileHandler,, yGrSize
          IF xGrSize > 0 AND yGrSize > 0 THEN
              xpos = 0
              ypos = 1
              DO
                GET #FileHandler,, DwordBuff
                IF (DwordBuff AND &b1000000000000000) = 0 THEN
                    RLEval = 1
                  ELSE
                    GET #FileHandler,, RLEval
                END IF
                PixVal.R = (((DwordBuff AND &b0111110000000000) SHR 10) * 8)
                PixVal.G = (((DwordBuff AND &b0000001111100000) SHR 5) * 8)
                PixVal.B = ((DwordBuff AND &b0000000000011111) * 8)
                IF (PixVal.R = 248) AND (PixVal.G = 0) AND (PixVal.B = 248) THEN ' Restore the "transparent" color (magenta).
                  PixVal.R = 255
                  PixVal.B = 255
                END IF
                FOR RepeatCount = 1 TO RLEval
                  IF xpos < xGrSize THEN
                      xpos += 1
                    ELSE
                      xpos = 1
                      ypos += 1
                  END IF
                  IF ypos <= yGrSize THEN
                      IF TargetImg = 0 THEN
                          PSET (xpos - 1, ypos - 1), RGB(PixVal.R, PixVal.G, PixVal.B)
                        ELSE
                          PSET TargetImg, (xpos - 1, ypos - 1), RGB(PixVal.R, PixVal.G, PixVal.B)
                      END IF
                    ELSE
                      Result = 3
                  END IF
                NEXT RepeatCount
                IF EOF(FileHandler) AND (xpos < xGrSize OR ypos < yGrSize) THEN
                  Result = 3
                END IF
              LOOP UNTIL ypos > yGrSize OR (ypos = yGrSize AND xpos = xGrSize) OR Result = 3
              IF NOT EOF(FileHandler) THEN Result = 4   ' We should be at EOF here. If not, the file is too long!
            ELSE
              Result = 3
          END IF
        ELSE
          Result = 2
      END IF
      CLOSE #FileHandler
    ELSE
      Result = 1
  END IF
  RETURN Result
END FUNCTION
