REM
REM This is the core of the BasMines game
REM It provides all the "logic" part, via the following procedures:
REM
REM
REM FUNCTION NewGameInit(NewGameWidth AS INTEGER, NewGameHeight AS INTEGER, NewGameMineNum AS INTEGER) AS INTEGER
REM   Initializes a new game.
REM   The function returns 0 on success, and -1 if something went wrong.
REM
REM FUNCTION GetField(x AS INTEGER, y AS INTEGER) AS INTEGER
REM   Returns the state of the field x,y as a byte. Possible states are:
REM    -64 Invalid field (out of the fieldpool)
REM    -3 Flagged field
REM    -2 Question-mark field
REM    -1 Hidden field
REM     0 Empty field
REM     1 Field "1"
REM     2 Field "2"
REM     n ...
REM     7 Field "7"
REM     8 Field "8"
REM     9 BOMB
REM    10 False bomb (empty field flagged as bomb after GameOver)
REM    11 Exploded Bomb (the bomb which blowed you out)
REM
REM FUNCTION SetField(x AS INTEGER, y AS INTEGER, s AS INTEGER) AS INTEGER
REM   Performs the s action on the field x,y. The function returns 0 on
REM   success, and -1 if something went wrong. s may be:
REM     0 Mark with a question mark
REM     1 Mark with a flag
REM     2 Unmark
REM     3 Discover the field
REM
REM FUNCTION GetGameDuration() AS INTEGER
REM   Returns the duration of the current game (in seconds). Returns -1 if
REM   no game has been initialized yet.
REM
REM FUNCTION GetGameOverFlag() AS BYTE
REM   -1: no game started  /  0: game in progress  /  1: Game Over (exploded!)  /  2: Game Over (Win!)
REM
REM FUNCTION GetFieldWidth() AS BYTE
REM   Returns the width of the minefield
REM
REM FUNCTION GetFieldHeight() AS BYTE
REM   Returns the height of the minefield
REM
REM FUNCTION GetMinesLeft() AS INTEGER
REM   Returns the number of mines left on the field (not flaged)
REM

TYPE MiningField
  Value AS BYTE  ' 0-9, where 9 = BOMB!
  PublicStatus AS BYTE  ' 0: Discovered, 1: Flag "?", 2: Flag "bomb", 3: Hidden, without flags.
END TYPE

DIM SHARED MineField(1 TO 64, 1 TO 64) AS MiningField
DIM SHARED AS INTEGER GameStartTime = -1, GameTimer, PausedStartTime, FieldWidth = -1, FieldHeight = -1, MinesLeft = -1, MinesTotal = -1
DIM SHARED AS BYTE GameOverFlag = -1  ' -1: no game started  /  0: game in progress  /  1: Game Over (lost)  /  2: Game Over (win)
DIM SHARED AS BYTE PauseFlag = 0  ' 0 = Game not paused, 1 = Game paused

RANDOMIZE TIMER, 3


PRIVATE SUB ClearMineField()
  DIM AS INTEGER x, y
  FOR x = 1 TO 64
    FOR y = 1 TO 64
      MineField(x, y).Value = 0
      MineField(x, y).PublicStatus = 3
    NEXT y
  NEXT x
END SUB


PRIVATE FUNCTION CountNeighBombs(x AS INTEGER, y AS INTEGER) AS BYTE
  DIM AS BYTE MineCount = 0
  IF y > 1 THEN                                             '  #x#
    IF MineField(x, y - 1).Value = 9 THEN MineCount += 1    '  #*#
  END IF                                                    '  ###
  IF y > 1 AND x < FieldWidth THEN                          '  ##x
    IF MineField(x + 1, y - 1).Value = 9 THEN MineCount += 1'  #*#
  END IF                                                    '  ###
  IF x < FieldWidth THEN                                    '  ###
    IF MineField(x + 1, y).Value = 9 THEN MineCount += 1    '  #*x
  END IF                                                    '  ###
  IF x < FieldWidth AND y < FieldHeight THEN                '  ###
    IF MineField(x + 1, y + 1).Value = 9 THEN MineCount += 1'  #*#
  END IF                                                    '  ##x
  IF y < FieldHeight THEN                                   '  ###
    IF MineField(x, y + 1).Value = 9 THEN MineCount += 1    '  #*#
  END IF                                                    '  #x#
  IF y < FieldHeight AND x > 1 THEN                         '  ###
    IF MineField(x - 1, y + 1).Value = 9 THEN MineCount += 1'  #*#
  END IF                                                    '  x##
  IF x > 1 THEN                                             '  ###
    IF MineField(x - 1, y).Value = 9 THEN MineCount += 1    '  x*#
  END IF                                                    '  ###
  IF x > 1 AND y > 1 THEN                                   '  x##
    IF MineField(x - 1, y - 1).Value = 9 THEN MineCount += 1'  #*#
  END IF                                                    '  ###
  RETURN MineCount
END FUNCTION

PRIVATE SUB AutoUndiscoverProcedure() ' Autodiscover all fields around public empty fields
  DIM AS INTEGER x, y
  DIM AS BYTE StatusChange
  DO
    StatusChange = 0
    FOR x = 1 TO FieldWidth
      FOR y = 1 TO FieldHeight
        IF MineField(x, y).Value = 0 AND MineField(x, y).PublicStatus = 0 THEN ' For each empty and discovered field, discover fields around (but not these which are flagged)
          IF x > 1 AND y > 1 AND MineField(x - 1, y - 1).PublicStatus = 3 THEN MineField(x - 1, y - 1).PublicStatus = 0 : StatusChange = 1
          IF y > 1 AND MineField(x, y - 1).PublicStatus = 3 THEN MineField(x, y - 1).PublicStatus = 0 : StatusChange = 1
          IF x < FieldWidth AND y > 1 AND MineField(x + 1, y - 1).PublicStatus = 3 THEN MineField(x + 1, y - 1).PublicStatus = 0 : StatusChange = 1
          IF x > 1 AND MineField(x - 1, y).PublicStatus = 3 THEN MineField(x - 1, y).PublicStatus = 0 : StatusChange = 1
          IF x < FieldWidth AND MineField(x + 1, y).PublicStatus = 3 THEN MineField(x + 1, y).PublicStatus = 0 : StatusChange = 1
          IF x > 1 AND y < FieldHeight AND MineField(x - 1, y + 1).PublicStatus = 3 THEN MineField(x - 1, y + 1).PublicStatus = 0 : StatusChange = 1
          IF y < FieldHeight AND MineField(x, y + 1).PublicStatus = 3 THEN MineField(x, y + 1).PublicStatus = 0 : StatusChange = 1
          IF x < FieldWidth AND y < FieldHeight AND MineField(x + 1, y + 1).PublicStatus = 3 THEN MineField(x + 1, y + 1).PublicStatus = 0 : StatusChange = 1
        END IF
      NEXT y
    NEXT x
  LOOP UNTIL StatusChange = 0
END SUB

FUNCTION GetGameOverFlag() AS BYTE
  RETURN GameOverFlag
END FUNCTION


FUNCTION GetFieldWidth() AS BYTE
  RETURN FieldWidth
END FUNCTION


FUNCTION GetFieldHeight() AS BYTE
  RETURN FieldHeight
END FUNCTION


SUB PauseGame() ' Pauses the game until it gets unpaused, or until the next setfield() call
  IF PauseFlag = 0 AND GameStartTime > 0 THEN ' Only if not paused yet, and the game already began
    PauseFlag = 1
    PausedStartTime = TIMER
  END IF
END SUB


SUB UnPauseGame() ' Unpauses the game (if paused), else does nothing
  IF PauseFlag = 1 THEN
    GameStartTime += (TIMER - PausedStartTime)
    GameTimer = TIMER
    PauseFlag = 0
  END IF
END SUB


FUNCTION NewGameInit(NewGameWidth AS INTEGER, NewGameHeight AS INTEGER, NewGameMineNum AS INTEGER) AS INTEGER
  DIM AS INTEGER x, y, z
  IF NewGameWidth < 1 OR NewGameHeight < 1 OR NewGameWidth * NewGameHeight <= NewGameMineNum THEN
    RETURN -1
  END IF
  PauseFlag = 0
  FieldWidth = NewGameWidth
  FieldHeight = NewGameHeight
  MinesTotal = NewGameMineNum
  GameOverFlag = 0  ' Clear out the GameOver flag
  ' Clear the entire minefield
  ClearMineField()
  ' Populate with some mines
  MinesLeft = 0
  DO
    x = INT(RND*FieldWidth) + 1
    y = INT(RND*FieldHeight) + 1
    IF MineField(x, y).Value = 0 THEN
      MineField(x, y).Value = 9
      MinesLeft += 1
    END IF
  LOOP UNTIL MinesLeft = MinesTotal
  ' Set all fields having mines as neighbours
  FOR x = 1 TO FieldWidth
    FOR y = 1 TO FieldHeight
      IF MineField(x, y).Value = 0 THEN
        MineField(x, y).Value = CountNeighBombs(x, y)
      END IF
    NEXT y
  NEXT x
  GameStartTime = 0
  RETURN 0
END FUNCTION


FUNCTION GetField(x AS INTEGER, y AS INTEGER) AS INTEGER
  IF x <= FieldWidth AND y <= FieldHeight AND x > 0 AND y > 0 THEN
      SELECT CASE MineField(x, y).PublicStatus
        CASE 0 ' Discovered field, return the real value
          RETURN MineField(x, y).Value
        CASE 1 ' Flagged field (?)
          RETURN -2
        CASE 2 ' Flagged field (bomb)
          RETURN -3
        CASE 3 ' Hidden field, no flags.
          RETURN -1
        CASE ELSE
          RETURN -63
      END SELECT
    ELSE
      RETURN -64
  END IF
END FUNCTION


FUNCTION SetField(x AS INTEGER, y AS INTEGER, s AS INTEGER) AS INTEGER
  DIM AS INTEGER xx, yy
  IF MineField(x, y).Value = 9 AND GameStartTime = 0 THEN ' If first clicked field is a bomb,
    WHILE MineField(x, y).Value = 9                       ' then init a new game, until the
      NewGameInit(FieldWidth, FieldHeight, MinesTotal)    ' field is not a bomb anymore.
    WEND
  END IF
  IF PauseFlag = 1 THEN UnPauseGame()   ' If the game is paused, unpause it
  IF GameStartTime = 0 THEN GameStartTime = TIMER : GameTimer = TIMER
  IF MineField(x, y).PublicStatus = 0 OR GameOverFlag > 0 THEN
      RETURN -1
    ELSE
      SELECT CASE s
        CASE 0 ' Mark with a question mark
          IF MineField(x, y).PublicStatus = 2 THEN MinesLeft += 1
          MineField(x, y).PublicStatus = 1
        CASE 1 ' Mark with a bomb flag
          IF MineField(x, y).PublicStatus <> 2 THEN
            MineField(x, y).PublicStatus = 2
            MinesLeft -= 1
          END IF
        CASE 2 ' Unmark
          IF MineField(x, y).PublicStatus = 2 THEN MinesLeft += 1
          MineField(x, y).PublicStatus = 3
        CASE 3 ' Discover the field
          MineField(x, y).PublicStatus = 0
          IF MineField(x, y).Value = 0 THEN AutoUndiscoverProcedure() ' Autodiscover all fields around public empty fields
          IF MineField(x, y).Value = 9 THEN
              MineField(x, y).Value = 11 ' set the bomb to "exploded bomb"
              FOR xx = 1 TO FieldWidth
                FOR yy = 1 TO FieldHeight
                  ' Discover all bombs
                  IF MineField(xx, yy).Value = 9 THEN MineField(xx, yy).PublicStatus = 0
                  ' Check all fields flagged as "bombs", and flag "false bombs" where there is no bomb (10)
                  IF MineField(xx, yy).PublicStatus = 2 THEN
                    MineField(xx, yy).PublicStatus = 0
                    MineField(xx, yy).Value = 10
                  END IF
                NEXT yy
              NEXT xx
              ' Set the GameOver flag
              GameOverFlag = 1
            ELSE ' Check if user cleared all fields
              GameOverFlag = 2 ' has the user won?
              FOR xx = 1 TO FieldWidth
                FOR yy = 1 TO FieldHeight
                  IF MineField(xx, yy).Value >= 0 AND MineField(xx, yy).Value <= 8 AND MineField(xx, yy).PublicStatus <> 0 THEN GameOverFlag = 0  ' nope, he hasn't
                NEXT yy
              NEXT xx
              IF GameOverFlag = 2 THEN  ' Flag all bombs
              FOR xx = 1 TO FieldWidth
                FOR yy = 1 TO FieldHeight
                  IF MineField(xx, yy).Value = 9 THEN MineField(xx, yy).PublicStatus = 2
                NEXT yy
              NEXT xx
              END IF
          END IF
      END SELECT
  END IF
  RETURN 0
END FUNCTION


FUNCTION GetGameDuration() AS INTEGER
  SELECT CASE GameStartTime
    CASE -1
      RETURN -1
    CASE 0
      RETURN 0
    CASE ELSE
      IF GameOverFlag = 0 THEN
        IF PauseFlag = 1 THEN
            GameTimer = PausedStartTime
          ELSE
            GameTimer = TIMER
        END IF
      END IF
      RETURN GameTimer - GameStartTime
  END SELECT
END FUNCTION


FUNCTION GetMinesLeft() AS INTEGER
  IF MinesLeft < 0 THEN
      RETURN 0
    ELSE
      RETURN MinesLeft
  END IF
END FUNCTION

